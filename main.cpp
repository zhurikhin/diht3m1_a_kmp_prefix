#include <iostream>
#include <vector>
#include <memory>

class ShiftsFunc
{
protected:
  std::vector<size_t> shifts_table;

public:
  virtual void load(std::string const& str) = 0;

  void load(std::vector<size_t> const& table)
  {
    shifts_table = table;
  }

  ShiftsFunc() = default;

public:
  size_t operator()(size_t index) const
  {
    return shifts_table.at(index);
  }
};

class PrefixFunc : public ShiftsFunc
{
  virtual void load(std::string const& str) final;

public:
  PrefixFunc() : ShiftsFunc() {};
  PrefixFunc(std::string const& str) { load(str); };
};

void PrefixFunc::load(std::string const& str)
{
  size_t str_len = str.length();
  shifts_table = std::vector<size_t>(str_len, 0);
  size_t max_prefix = 0;

  for (size_t s = 1; s < str_len; ++s)
  {
    while (max_prefix > 0 and str[max_prefix] != str[s])
      max_prefix = shifts_table[max_prefix - 1];
    if (str[max_prefix] == str[s])
      ++max_prefix;
    shifts_table[s] = max_prefix;
  }
}

class KMPSearch
{
  std::unique_ptr<ShiftsFunc> shifts_fp;

public:
  KMPSearch(std::unique_ptr<ShiftsFunc>&& shifts_)
    : shifts_fp(std::move(shifts_))
  {}

  std::vector<size_t> operator() (std::string const& pattern,
      std::string const& text)
  {
    ShiftsFunc& shifts = *shifts_fp;
    shifts.load(pattern);

    std::vector<size_t> occurences;

    size_t ptn_length = pattern.length();
    size_t txt_length = text.length();

    size_t max_match = 0;
    for (size_t i = 0; i < txt_length; ++i)
    {
      while(max_match > 0 and pattern[max_match] != text[i])
        max_match = shifts(max_match - 1);
      if (pattern[max_match] == text[i])
        ++max_match;

      if (max_match == ptn_length)
      {
        occurences.push_back(i + 1 - ptn_length);
        max_match = shifts(max_match - 1);
      }
    }

    return occurences;
  }
};

int main()
{
  std::string pattern;
  std::string text;

  std::getline(std::cin, pattern);
  std::getline(std::cin, text);

  auto search = KMPSearch(std::make_unique<PrefixFunc>());
  std::vector<size_t> occurences = search(pattern, text);

  for (size_t o : occurences)
    std::cout << o << ' ';
  std::cout << std::endl;

  return 0;
}